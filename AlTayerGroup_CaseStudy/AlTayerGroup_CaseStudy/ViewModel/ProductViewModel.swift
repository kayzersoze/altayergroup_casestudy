//
//  ProductViewModel.swift
//  AlTayerGroup_CaseStudy
//
//  Created by Serhat Sezer on 16/07/2017.
//  Copyright © 2017 Serhat Sezer. All rights reserved.
//

import Foundation

struct ProductViewModel {
    
    fileprivate var items: [ProductItem]!
    
    var totalItems: Int {
        return items.count
    }
    
    init(items: [ProductItem]) {
        self.items = items
    }
    
    func product(at: IndexPath) -> ProductItem {
        return items[at.row]
    }
}
