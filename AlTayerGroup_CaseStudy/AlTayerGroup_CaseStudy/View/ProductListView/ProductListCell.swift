//
//  ProductListCell.swift
//  AlTayerGroup_CaseStudy
//
//  Created by Serhat Sezer on 18/07/2017.
//  Copyright © 2017 Serhat Sezer. All rights reserved.
//

import UIKit
import TTTAttributedLabel

class ProductListCell: UITableViewCell {
    
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productTitleLabel: UILabel!
    @IBOutlet weak var productDescriptionLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
   
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(model: ProductItem) {
        productTitleLabel.text = model.name
        let attrString = NSAttributedString(string: String.stringFromHtml(string: model.desc)!.string
            , attributes: [
                NSFontAttributeName: UIFont.systemFont(ofSize: 14)
            
            ])
        productDescriptionLabel.attributedText = attrString
        priceLabel.text = "\(model.price) $"
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    static var cellId: String {
        return String(describing: self)
    }
    
    static var cellHeight: CGFloat {
        return CGFloat(110.0)
    }
}

