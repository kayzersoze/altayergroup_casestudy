//
//  ProductListView.swift
//  AlTayerGroup_CaseStudy
//
//  Created by Serhat Sezer on 16/07/2017.
//  Copyright © 2017 Serhat Sezer. All rights reserved.
//

import UIKit

protocol CustomCell {
    var cellId: String { get }
    var cellHeight: CGFloat { get }
}

class ProductListView: UIView {
    let navigationBarHeight: CGFloat = 60.0
    
    fileprivate var tableView: UITableView!
    fileprivate var tableViewFrame: CGRect!
    
    var viewModel: ProductViewModel! {
        didSet {
            tableView.reloadData()
        }
    }
    
    var clickBlock : ((IndexPath) -> Void)!
    
    init(viewModel: ProductViewModel) {
        super.init(frame: .zero)
        self.viewModel = viewModel
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.tableViewFrame = frame
        commonInit()
    }
    
    convenience init() {
        self.init(frame: .zero)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    public func registerCustomCell(cell: CustomCell) {
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cell.cellId)
    }
}

extension ProductListView {
    func commonInit() {
        tableView = UITableView()
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = ProductListCell.cellHeight

        tableView.frame = CGRect(x: tableViewFrame.minX, y: tableViewFrame.minY + navigationBarHeight,
                                 width: tableViewFrame.width, height: tableViewFrame.height)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: ProductListCell.cellId, bundle: nil),
                           forCellReuseIdentifier: ProductListCell.cellId)
        addSubview(tableView)
    }
}

extension ProductListView: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.totalItems
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ProductListCell.cellId, for: indexPath) as! ProductListCell
        cell.configure(model: viewModel.product(at: indexPath))
        return cell
    }
}

extension ProductListView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.clickBlock(indexPath)
    }
}
