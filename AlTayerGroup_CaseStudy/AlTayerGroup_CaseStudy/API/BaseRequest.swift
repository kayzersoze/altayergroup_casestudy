//
//  BaseRequest.swift
//  AlTayerGroup_CaseStudy
//
//  Created by Serhat Sezer on 24/07/2017.
//  Copyright © 2017 Serhat Sezer. All rights reserved.
//

import Foundation
import Alamofire

public class BaseRequest {
    
    public var httpMethod: HTTPMethod  = .get
    public var path: String = "/"
    public var parameters: [String : Any] = [:]
}
