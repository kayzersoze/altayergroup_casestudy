//
//  AlamofireExtension.swift
//  AlTayerGroup_CaseStudy
//
//  Created by Serhat Sezer on 18/07/2017.
//  Copyright © 2017 Serhat Sezer. All rights reserved.
//

import Foundation
import ObjectMapper
import Alamofire
import SwiftyJSON

extension DataRequest {
    //Response Object - Serialize the response from JSON to T
    @discardableResult
    func responseObject<T: Mappable>(_ completionHandler: @escaping (URLRequest?, HTTPURLResponse?, T?, Error?) -> Void) -> Self {
        return responseData(completionHandler: { dataResponse in
            guard let data = dataResponse.result.value, let json = try? JSONSerialization.jsonObject(with: data, options: []) else {
                completionHandler(dataResponse.request, dataResponse.response, nil, dataResponse.error)
                return
            }
            
            DispatchQueue.global(qos: .default).async {
                let object = Mapper<T>().map(JSONObject: json)
                DispatchQueue.main.async {
                    completionHandler(dataResponse.request, dataResponse.response, object, dataResponse.error)
                }
            }
        })
    }
    
    @discardableResult
    func responseObject<T: Mappable>(_ completionHandler: @escaping (T?, Error?) -> Void) -> Self {
        return responseObject({ (_, _, object, error) in
            completionHandler(object, error)
        })
    }
    
    //    //Response Array - Serialize the response from JSON to [T]
    @discardableResult
    func responseArray<T: Mappable>(_ completionHandler: @escaping (URLRequest?, HTTPURLResponse?, [T]?, Error?) -> Void) -> Self {
        return responseData(completionHandler: { dataResponse in
            guard let data = dataResponse.result.value, let json = try? JSONSerialization.jsonObject(with: data, options: []) else {
                completionHandler(dataResponse.request, dataResponse.response, nil, dataResponse.error)
                return
            }
            
            DispatchQueue.global(qos: .default).async {
                let object = Mapper<T>().mapArray(JSONObject: json)
                DispatchQueue.main.async {
                    completionHandler(dataResponse.request, dataResponse.response, object, dataResponse.error)
                }
            }
        })
    }
    
    @discardableResult
    func responseArray<T: Mappable>(_ completionHandler: @escaping ([T]?, Error?) -> Void) -> Self {
        return responseArray { (_, _, object, error) -> Void in
            completionHandler(object, error)
        }
    }
    
    //Response Base Response Array - Serialize the response at the "data" key of the top level JSON to [T]
    @discardableResult
    func responseBaseResponseArray<T: Mappable>(_ completionHandler: @escaping (URLRequest?, HTTPURLResponse?, [T]?, Error?) -> Void) -> Self {
        return responseData(completionHandler: { dataResponse in
            guard let data = dataResponse.result.value, let json = try? JSONSerialization.jsonObject(with: data, options: []), let jsonObject = json as? [String: Any] else {
                completionHandler(dataResponse.request, dataResponse.response, nil, dataResponse.error)
                return
            }
            
            DispatchQueue.global(qos: .default).async {
                let object = Mapper<T>().mapArray(JSONObject: jsonObject["data"])
                DispatchQueue.main.async {
                    completionHandler(dataResponse.request, dataResponse.response, object, dataResponse.error)
                }
            }
        })
    }
    
    @discardableResult
    func responseBaseResponseArray<T: Mappable>(_ completionHandler: @escaping ([T]?, Error?) -> Void) -> Self {
        return responseBaseResponseArray { (_, _, object, error) -> Void in
            completionHandler(object, error)
        }
    }
    
    @discardableResult
    func responseSwiftyJSON(_ completionHandler: @escaping (URLRequest?, HTTPURLResponse?, SwiftyJSON.JSON?, Error?) -> Void) -> Self {
        return responseData(completionHandler: { dataResponse in
            guard let data = dataResponse.result.value else {
                completionHandler(dataResponse.request, dataResponse.response, nil, dataResponse.error)
                return
            }
            
            DispatchQueue.global(qos: .default).async {
                let object = SwiftyJSON.JSON(data: data)
                DispatchQueue.main.async {
                    completionHandler(dataResponse.request, dataResponse.response, object, dataResponse.error)
                }
            }
        })
    }
    
    @discardableResult
    func responseSwiftyJSON(_ completionHandler: @escaping (SwiftyJSON.JSON?, Error?) -> Void) -> Self {
        return responseSwiftyJSON({ (_, _, json, error) -> Void in
            completionHandler(json, error)
        })
    }
}

extension DataRequest {
    func responseDebugPrint() -> Self {
        return responseJSON() {
            response in
            if let  JSON = response.result.value,
                let JSONData = try? JSONSerialization.data(withJSONObject: JSON, options: .prettyPrinted),
                let prettyString = String(data: JSONData, encoding: .utf8) {
            } else if let error = response.result.error {
                print("Error Debug Print: \(error.localizedDescription)")
            }
        }
    }
}
