//
//  APIManager.swift
//  AlTayerGroup_CaseStudy
//
//  Created by Serhat Sezer on 18/07/2017.
//  Copyright © 2017 Serhat Sezer. All rights reserved.
//
import Foundation
import Alamofire


public struct APIManager {
    public static let shared: APIManager = APIManager()
    
    /// Calls completion block when there is a data or call error.
    ///
    /// - Parameters:
    ///   - name: Brand name
    ///   - completion: Holds brand information or error data
    func getProducts(searchString: String, page: Int = 0, size: Int = 10, completion: @escaping (_ brands: ProductResponse?, _ error: Error?) -> ()){
        
        let request = BaseRequest()
        request.httpMethod = .post
       // let requestPath = "/search/full/?searchString=\(searchString)&page=\(page)&hits=\(size)"
        let requestPath = "/search/full/"
        request.path = requestPath
        request.parameters = ["searchString": searchString, "page": page, "hits": size]
        
        
        Alamofire.request(Router.allProducts(request)).responseObject { (brandResponse: ProductResponse?, error) in
            completion(brandResponse, error)
        }
        
    }
}


fileprivate enum Router: URLRequestConvertible {
    static fileprivate let testURL: URL = URL(string:"https://www.mamasandpapas.ae")!
    static fileprivate let prodURL: URL = URL(string:"https://www.mamasandpapas.ae")!
    static fileprivate let API_KEY: String = "C6490912AB3211E680F576304DEC7EB7"
    
    case brands(name: String)
    case customRequest(BaseRequest)
    case allProducts(BaseRequest)
    
    fileprivate var method: HTTPMethod {
        switch self {
        case .customRequest(let req):
            return req.httpMethod
        case .allProducts(let productReq):
            return productReq.httpMethod
        default:
            return .get
        }
    }
    
    fileprivate var path: String {
        switch self {
        case .customRequest(let req):
            return req.path
        case .allProducts(let productReq):
            return productReq.path
        case .brands(let brandName):
            return "\(brandName)"
        }
    }
    
    public func asURLRequest() throws -> URLRequest {
        let URL: URL
        
        if let _ = ProcessInfo.processInfo.environment["TEST_SERVER"] {
            URL = Router.testURL
        } else {
            URL = Router.prodURL
        }
        
        var mutableURLRequest = URLRequest(url: URL.appendingPathComponent(path))
        mutableURLRequest.httpMethod = method.rawValue
        mutableURLRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        switch self {
        case .customRequest(let req):
            if req.httpMethod == .post {
                return try! JSONEncoding.default.encode(mutableURLRequest, with: nil)
            }
            
        case .allProducts(let productReq):
            if productReq.httpMethod == .post {
                let encodedReq = try! URLEncoding.queryString.encode(mutableURLRequest, with: productReq.parameters)
                return encodedReq
                
            }
        case .brands(_):
            return try URLEncoding.queryString.encode(mutableURLRequest, with: ["apiKey": Router.API_KEY])
        }
        return mutableURLRequest
    }
    
    
}

