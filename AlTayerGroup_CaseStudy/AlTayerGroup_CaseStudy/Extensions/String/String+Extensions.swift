//
//  String+Extensions.swift
//  AlTayerGroup_CaseStudy
//
//  Created by Serhat Sezer on 26/07/2017.
//  Copyright © 2017 Serhat Sezer. All rights reserved.
//

import Foundation
import UIKit

extension String {
    
    static func stringFromHtml(string: String) -> NSAttributedString? {
        do {
            let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
            if let d = data {
                let str = try NSAttributedString(data: d,
                                                 options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                 documentAttributes: nil)
                return str
            }
        } catch {
        }
        return nil
    }
}
