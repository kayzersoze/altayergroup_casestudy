//
//  ViewController.swift
//  AlTayerGroup_CaseStudy
//
//  Created by Serhat Sezer on 16/07/2017.
//  Copyright © 2017 Serhat Sezer. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    let indicatorView = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        indicatorView.center = view.center
        indicatorView.startAnimating()
        view.addSubview(indicatorView)
        
        APIManager.shared.getProducts(searchString: "boy", page: 1, size: 10) { productResponse, error in
            if let response = productResponse {
                self.indicatorView.removeFromSuperview()
                self.addListView(products: response.items)
            }
        }
    }
    
    func addListView(products: [ProductItem]) {
        let productListViewModel = ProductViewModel(items: products)
        
        let productTableView = ProductListView(frame: CGRect(x: 0, y:0, width: view.frame.width, height: view.frame.height))
        productTableView.viewModel = productListViewModel

        productTableView.clickBlock = { indexpath in
            print("table 2 \(indexpath.row)")
        }
        view.addSubview(productTableView)
    }
}

