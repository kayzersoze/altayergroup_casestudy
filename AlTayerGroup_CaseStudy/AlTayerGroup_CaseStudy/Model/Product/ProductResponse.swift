//
//  Product.swift
//  AlTayerGroup_CaseStudy
//
//  Created by Serhat Sezer on 16/07/2017.
//  Copyright © 2017 Serhat Sezer. All rights reserved.
//

import Foundation
import ObjectMapper

final class ProductResponse: BaseResponse {
    
    var items: [ProductItem] = []
    
    override func mapping(map: Map) {
        // we have to override to get basic attributes.
        super.mapping(map: map)
        items <- map["hits"]
    }
}
