//
//  Product.swift
//  AlTayerGroup_CaseStudy
//
//  Created by Serhat Sezer on 16/07/2017.
//  Copyright © 2017 Serhat Sezer. All rights reserved.
//

import Foundation
import ObjectMapper

struct ProductItem: Mappable {
    var id: Int = 0
    var name: String = ""
    var desc: String = ""
    var price: Int = 0
    var media = [MediaItem]()
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        id <- map["productId"]
        name <- map["name"]
        desc <- map["description"]
        price <- map["price"]
        media <- map["media"]
    }
}


struct MediaItem: Mappable {
    var position: Int = 0
    var mediaType: String = ""
    var src: String = ""
    var videoUrl: String = ""
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        position <- map["position"]
        mediaType <- map["mediaType"]
        src <- map["src"]
        videoUrl <- map["videoUrl"]
    }
}

extension MediaItem {
    func imagePath() -> String? {
        guard mediaType == "image" else {
            return nil
        }
        return "https://www.mamasandpapas.ae\(src)"
    }
}
