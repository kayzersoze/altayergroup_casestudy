//
//  Product.swift
//  AlTayerGroup_CaseStudy
//
//  Created by Serhat Sezer on 16/07/2017.
//  Copyright © 2017 Serhat Sezer. All rights reserved.
//

import Foundation
import ObjectMapper

class BaseResponse: Mappable {
    
    var pagination: Pagination?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        pagination <- map["pagination"]
        
    }
    
}

class Pagination: Mappable {
    var totalHints: Int = 0
    var totalPages: Int = 0
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        totalHints <- map["totalHints"]
        totalPages <- map["totalPages"]
    }
    
}
